# docker-compose-application

This template docker-compose configuration reflects the standard setup at
Naturalis for web services deployed with docker-compose.

Use
[ansible-role-docker-compose](https://gitlab.com/naturalis/lib/ansible/ansible-role-docker-compose)
to start and manage your docker-compose application using Ansible.
